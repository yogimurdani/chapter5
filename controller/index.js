const Home = require("./home")
const Games = require("./games")
const User = require("./user")

module.exports = {
  Home, Games, User
}