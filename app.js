const express = require("express");
const path = require("path");
const Middleware = require("./middleware/index")
const port = 3000
const routes = require("./routes/routes");
const app = express();

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

app.use(routes);
app.use(Middleware.errorHandler)

app.all("*", (req, res) => {
  res.send("404 Page Not Found")
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

module.exports = app;
