const express = require("express");
const app = express();
const {Home, Games, User} = require("../controller")
const Middleware = require("../middleware")

app.use(Middleware.putMiddleware, Home, Games, User)

module.exports = app;